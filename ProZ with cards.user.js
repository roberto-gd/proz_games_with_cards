// ==UserScript==
// @name         Pro-Z games with cards
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Highlights games with cards. Works 99% of the time (sometimes the same game has different AppIDs). Doesn't work with packages or DLCs
// @author       Robert
// @match        *://pro-z.org/market
// @connect      steamcardexchange.net
// @require      https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js
// @grant        GM_xmlhttpRequest
// @downloadURL  https://bitbucket.org/roberto-gd/proz_games_with_cards/raw/master/ProZ%20with%20cards.user.js
// ==/UserScript==

SCE_LINK = function ()
{
    return "http://www.steamcardexchange.net/index.php?inventorygame-appid-";
};

$(document).ready(function() {
    addButton();
});

function addButton ()
{
    $('.nested.vertical.menu').append('<li id="MyListItem"><a><i class="fa fa-tag"></i>Games with cards</a><input type="button" value="Check" id="CP"></li>');
    $('#CP').click(function()
    {
        resetCountInfo();
        var n = 0;
        $('.button.medium.success').each(function (index, element)
        {
            if(!$(this).parent().parent().hasClass('filter-letter') && !$(this).parent().parent().hasClass('filter-owned'))
             {
                 incrementTotalGamesToCheck();
                 var data = $(this).attr("href").split('/');
                 var gameId = data[data.length-1];
                 //check if we already know about the current game
                 var info = localStorage.getItem(gameId);
                 if(info !== null)
                 {
                     console.log("we already know about "+gameId);
                     if(info == "Y")
                     {
                         console.log("...and it does have cards");
                         addLinkToElement(element, SCE_LINK()+gameId);
                         addHasCardsClassToElement(element);
                     }
                     incrementGamesCheckedCount();
                 }
                 else
                 {
                     //find out
                     //highlightGameWithCards(gameId, element);
                     var waitingTime = 1000;
                     setTimeout(function(){highlightGameWithCards(gameId, element);}, waitingTime*n);
                     n++;
                 }
             }
        });
    });
    addInfoText();
    addVisibilityCheckbox();
}

// ******** Info about game count ******** //
window.gamesCheckedCount = 0;
window.totalGamesToCheck = 0;
addInfoText = function  ()
{
    $('#MyListItem').append('<p id="countInfo">Games checked: <span id="gamesCheckedCount">0</span> / <span id="totalGamesToCheck">0</span></p>');
    $("#countInfo").css("color", "white");
};

addVisibilityCheckbox = function  ()
{
    $('#countInfo').append('<br><label style="color:white;"><input type="checkbox" name="gamesVisibility">Show only games with cards</input></label>');
    $('input[name="gamesVisibility"]').on('click', function(){
    if ( $(this).is(':checked') ) {
        $('.row.game').each(function (index, element)
        {
            if(!$(this).hasClass('hasCards')) {
                $(this).addClass('filter-NoCards');
            }
        });
    }
    else {
        $('.row.game').each(function (index, element)
        {
            $(this).removeClass('filter-NoCards');
        });
    }
});
};

addHasCardsClassToElement = function (element)
{
    $(element).parent().parent().addClass("hasCards");
};

updateUICountInfo = function()
{
    $("#gamesCheckedCount").text(gamesCheckedCount.toString());
    $("#totalGamesToCheck").text(totalGamesToCheck.toString());
    if(totalGamesToCheck !== 0 && gamesCheckedCount === totalGamesToCheck)
    {
        $("#countInfo").css("color", "#00ff00");
    }
    else
    {
        $("#countInfo").css("color", "white");
    }
};

incrementGamesCheckedCount = function()
{
    gamesCheckedCount++;
    updateUICountInfo();
};

incrementTotalGamesToCheck = function()
{
    totalGamesToCheck++;
    updateUICountInfo();
};

resetCountInfo = function()
{
    gamesCheckedCount = 0;
    totalGamesToCheck = 0;
    updateUICountInfo();
};
// *************************************** //

highlightGameWithCards = function (appId, element)
{
    console.log("Checking if game "+appId+" has cards...");
    var link = SCE_LINK()+appId;


    GM_xmlhttpRequest({
        method: "GET",
        timeout: 10000,
        url: link,
        onload: function(data)
        {
            var parser = new DOMParser ();
            var responseDoc = parser.parseFromString (data.responseText, "text/html");
            if(responseDoc.getElementById('inventory-game-info') !== null)
            {
                addLinkToElement(element, link);
                localStorage.setItem(appId, "Y");
                addHasCardsClassToElement(element);
            }
            else
            {
                if(data.responseText.indexOf("Game not found")!==-1)
                {
                    console.log(appId+" doesn't have cards!");
                    localStorage.setItem(appId, "N");
                }
                //<h1>503 Service Temporarily Unavailable</h1>
                //TODO: handle 503. maybe we got a 503 error and we need to try again. this happens with multiple request in a very short time
                else if(data.responseText.indexOf("503")!==-1)
                {
                    console.log("Request "+appId+": 503 Service Temporarily Unavailable");
                    addErrorToElement(element, "Error 503");
                }
                else
                {
                    addErrorToElement(element, "Unknown error");
                }
            }
            incrementGamesCheckedCount();
        },
        ontimeout: function(data)
        {
            console.log("[Pro-Z games with cards] Request " + link + " Timeout");
            addErrorToElement(element, "Timeout");
        }
    });
};

addLinkToElement = function (element, link)
{
    var a = document.createElement('a');
    var linkText = document.createTextNode("Visit SCE page");
    a.appendChild(linkText);
    a.href = link;
    element.parentElement.appendChild(a);
};

addErrorToElement = function (element, error)
{
    var span = document.createElement('span');
    var textNode = document.createTextNode(error);
    span.style.color = "red";
    span.appendChild(textNode);
    element.parentElement.appendChild(span);
};