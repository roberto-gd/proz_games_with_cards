# Pro-Z: Highlight games with cards

Helper script for Tampermonkey which is used on Pro-Z website (market page) to show which games have Steam trading cards.

Basic functionality. Works most of the time (sometimes the same game has different AppIDs). It won't work properly with packages or DLCs.

Feel free to use this as a starting point for your own custom scripts.